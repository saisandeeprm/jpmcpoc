package virtusa.poc.jpmc;

import java.io.InputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Component

@Service
public class ContractRoomConnectorServiceImpl implements ContractRoomConnectorService{
	private static final Logger log = LoggerFactory.getLogger(HtmlExporter.class);
	private HttpClient client = null;
	private RequestEntity body = null;
	private String token = StringUtils.EMPTY;
	private JSONObject jsonContentBody =null;
	private String AgreementCid =StringUtils.EMPTY;

	public ContractRoomConnectorServiceImpl() {
		client=new HttpClient();
	}

	private void getToken() {
		try {
			log.info("came into getToken method");

			jsonContentBody = new JSONObject();

			jsonContentBody.put("api_key", "saisandeeprm@virtusapolaris.com");
			jsonContentBody.put("secret", "328f83c13007345cbe9744af3bc0baa8f14fe292");
			body = new StringRequestEntity(jsonContentBody.toString(), "application/json", "UTF-8");
			PostMethod request = new PostMethod("https://demo.contractroom.com/rest/v2/obtain-token/");

			request.setRequestEntity(body);

			int status = client.executeMethod(request);
			if (status != 200) {
				log.info("getTokenMethod::");
				throw new RuntimeException("Failed : HTTP error code : " + status);
			}

			InputStream responseBodyAsStream = request.getResponseBodyAsStream();

			JSONObject json = new JSONObject(IOUtils.toString(responseBodyAsStream, "UTF-8"));
			token = json.get("token").toString();
			log.info("getTokenMethod::token is==" + token);

		}

		catch (Exception e) {
			log.info("came into getToken Exception" + e.getMessage());

		}

	}

	private void createAgreement(String agreementName, int frameworkNumber) {
		try {
			jsonContentBody = new JSONObject();
			jsonContentBody.put("name", agreementName);
			jsonContentBody.put("framework", frameworkNumber);

			body = new StringRequestEntity(jsonContentBody.toString(), "application/json", "UTF-8");
			PostMethod request = new PostMethod("https://demo.contractroom.com/rest/v2/agreement/");
			request.setRequestHeader("Authorization", "Token " + token);
			request.setRequestHeader("Accept", "application/json");
			request.setRequestEntity(body);
			int status = client.executeMethod(request);
			if (status != 201) {
				log.info("CreateAgreement::");
				throw new RuntimeException("Failed : HTTP error code : " + status);
			}

			InputStream responseBodyAsStream = request.getResponseBodyAsStream();
			JSONObject json = new JSONObject(IOUtils.toString(responseBodyAsStream, "UTF-8"));
			log.info("json object of createAgreement"+json);
			AgreementCid = json.get("cid").toString();
			log.info("aggrement is created with cid" + AgreementCid);

		} catch (Exception E) {
			log.info("came into createAgreement method Exception" + E.getMessage());

		}

	}

	private String getAgreementSection() {
		String cidSection = null;
		try {
			GetMethod request = new GetMethod(
					"https://demo.contractroom.com/rest/v2/agreement/" + AgreementCid + "/content/");
			request.setRequestHeader("Authorization", "Token " + token);
			request.setRequestHeader("Accept", "application/json");

			int status = client.executeMethod(request);
			if (status != 200) {
				log.info("CreateAgreement::");
				throw new RuntimeException("Failed : HTTP error code : " + status);
			}

			InputStream responseBodyAsStream = request.getResponseBodyAsStream();
			JSONObject json = new JSONObject(IOUtils.toString(responseBodyAsStream, "UTF-8"));

			JSONArray jsonArray = json.getJSONArray("tags");

			if (jsonArray.length() > 1) {
				String jsonstring = jsonArray.getString(0);
				JSONObject json2 = new JSONObject(jsonstring);
				log.info("cme into the if" + json2);
				cidSection = json2.get("cid").toString();
			}

			log.info("cid value of the section is " + cidSection);
			return cidSection;
		}

		catch (Exception E) {
			log.info("came into getAgreementSection method Exception" + E.getMessage());

		}
		return cidSection;

	}

	private void updateSectionContent(String cidSection, String contentFragment) {
		try {
			jsonContentBody = new JSONObject();
			jsonContentBody.put("content", contentFragment);
			jsonContentBody.put("type", "raw");
			PutMethod request = new PutMethod("https://demo.contractroom.com/rest/v2/agreement/" + AgreementCid
					+ "/section/" + cidSection + "/content/");

			body = new StringRequestEntity(jsonContentBody.toString(), "application/json", "UTF-8");
			request.setRequestHeader("Authorization", "Token " + token);
			request.setRequestHeader("Accept", "application/json");
			request.setRequestEntity(body);
			int status = client.executeMethod(request);
			if (status != 200) {
				log.info("getTokenMethod::");
				throw new RuntimeException("Failed : HTTP error code : " + status);
			}

			InputStream responseBodyAsStream = request.getResponseBodyAsStream();

			JSONObject json = new JSONObject(IOUtils.toString(responseBodyAsStream, "UTF-8"));

			log.info("updated section respose" + json.toString());
		} catch (Exception E) {
			log.info("came into Section method Exception" + E.getMessage());

		}

	}

	public void connector() {
		getToken();
		createAgreement("AgreementFromAem", 7096);
		String SectionCid = getAgreementSection();
		updateSectionContent(SectionCid, "<p>came from eclipse</p>");
}

}
