package virtusa.poc.jpmc;



import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import org.apache.sling.engine.SlingRequestProcessor;
import org.apache.sling.settings.SlingSettingsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import java.io.ByteArrayOutputStream;

import java.util.Collections;

import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import aQute.bnd.annotation.ProviderType;

@ProviderType
@Component
@Service
@org.apache.felix.scr.annotations.Property(name="process.label", value={"Export As Html"})

public class HtmlExporter implements WorkflowProcess{

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private RequestResponseFactory requestResponseFactory;

    /** Service to process requests through Sling */
    @Reference
    private SlingRequestProcessor requestProcessor;

    @Reference
	private SlingSettingsService slingSettingsService;

	@Reference
	private ContractRoomConnectorService contractRoomConnectorService;

	  private static final Logger log = LoggerFactory.getLogger(HtmlExporter.class);


	  private Set<String> runModes = null;
	  private static final String PUBLISH = "publish";
	  public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args)
		    throws WorkflowException
		  {

		final WorkflowData workflowData = workItem.getWorkflowData();

		final String type = workflowData.getPayloadType();

        // Check if the payload is a path in the JCR
        if (!StringUtils.equals(type, "JCR_PATH")) {
            return;
        }
        String payloadPath= (workflowData.getPayload().toString()).concat(".html");
        final ResourceResolver resolver;
		try{
			resolver = resourceResolverFactory.getResourceResolver(Collections.singletonMap("user.jcr.session",
                    (Object) workflowSession.getSession()));

        HttpServletRequest req = requestResponseFactory.createRequest("GET", payloadPath);
        WCMMode.DISABLED.toRequest(req);

       ByteArrayOutputStream out = new ByteArrayOutputStream();
        HttpServletResponse resp = requestResponseFactory.createResponse(out);

        requestProcessor.processRequest(req, resp, resolver);

        String html = out.toString();

        Externalizer externalizer =resolver.adaptTo(Externalizer.class);



String regex="(src*=*\"(.+?)\")|(href*=*\"(.+?)\")";
Matcher m = Pattern.compile("(?=(" + regex + "))").matcher(html);
Set<String> matchStrings=new TreeSet<String>();
while(m.find()) {

   String group = m.group(1).substring(m.group(1).indexOf("\"")+1, m.group(1).lastIndexOf("\""));
   if(!matchStrings.contains(group)) {

	   matchStrings.add(group);
	   String Link ;
if(!isPublishInstance()){
	Link=  externalizer.authorLink(resolver,group);

		 html=html.replace(group, Link);
		 html=html.replaceAll("%25","%");
}
else
{
	Link=  externalizer.publishLink(resolver,group);
	html=html.replace(group, Link);
	html=html.replaceAll("%25","%");
}

   }

}


log.info("html generated"+html);
contractRoomConnectorService.connector();


		}
		catch(Exception e)
		{
log.info("HtmlExporter exception"+e.getMessage());
		}

}

	private boolean isPublishInstance()
	{
		if(runModes == null)
		{
			runModes = slingSettingsService.getRunModes();
		}
		return runModes.contains(PUBLISH);
	}
}