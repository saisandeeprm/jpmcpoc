package virtusa.poc.jpmc;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;

import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.nodetype.NodeType;

import org.apache.commons.lang.StringUtils;
import aQute.bnd.annotation.ProviderType;

@ProviderType
@Component
@Service
@org.apache.felix.scr.annotations.Property(name = "process.label", value = { "Export As ContentFragment" })

public class ExportContentFragment implements WorkflowProcess {

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Reference
	private ContractRoomConnectorService contractRoomConnectorService;

	private static final Logger log = LoggerFactory.getLogger(HtmlExporter.class);

	public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args) throws WorkflowException {
		log.info("came into the excute method");

		final WorkflowData workflowData = workItem.getWorkflowData();

		final String type = workflowData.getPayloadType();

		// Check if the payload is a path in the JCR
		if (!StringUtils.equals(type, "JCR_PATH")) {
			return;
		}
		final ResourceResolver resourceResolver;
		try {
			resourceResolver = resourceResolverFactory.getResourceResolver(
					Collections.singletonMap("user.jcr.session", (Object) workflowSession.getSession()));

			Session session = workflowSession.getSession();
			javax.jcr.Node node = session.getNode((workflowData.getPayload().toString()));
			NodeType primaryNodeType = node.getPrimaryNodeType();
			String orderFolder = primaryNodeType.toString();
			StringBuilder s = new StringBuilder();

			if (orderFolder.equals("sling:Folder")) {
				NodeIterator nodes = node.getNodes();

				while (nodes.hasNext()) {
					log.info("came into while node");

					javax.jcr.Node nextNode = nodes.nextNode();
					if (nextNode.getPrimaryNodeType().toString().equals("dam:Asset")) {
						log.info("nodepath" + nextNode.getPath());
						Resource res = resourceResolver.getResource(nextNode.getPath());
						Asset asset = res.adaptTo(Asset.class);
						InputStream data = asset.getOriginal().getStream();
						InputStreamReader reader = new InputStreamReader(data);
						BufferedReader br = new BufferedReader(reader);
						String line;
						while ((line = br.readLine()) != null) {
							s.append(line);

						}

					}
				}

			} else {
				Resource res = resourceResolver.getResource((workflowData.getPayload().toString()));
				Asset asset = res.adaptTo(Asset.class);
				InputStream data = asset.getOriginal().getStream();
				InputStreamReader reader = new InputStreamReader(data);
				BufferedReader br = new BufferedReader(reader);
				String line;
				while ((line = br.readLine()) != null) {
					s.append(line);

				}
			}
			contractRoomConnectorService.connector();
			log.info("came into the primaryNodeType " + s.toString());

		} catch (Exception e) {
			log.info("HtmlExporter exception" + e.getMessage());
		}

	}

}